FROM python:3.6

ENV PYTHONUNBUFFERED 1
RUN mkdir /code
WORKDIR /code
COPY . /code/

RUN  pip install --default-timeout=100 -r requirements.txt
EXPOSE 8080
CMD ["python","/code/manage.py","runserver","0.0.0.0:8080"]