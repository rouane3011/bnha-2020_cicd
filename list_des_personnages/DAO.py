import json 
from django.conf import settings
import os

class heroDAO:
    liste = None

    @staticmethod
    def get_hero():
        with open(os.path.join(settings.STATICFILES_DIRS[0], "list", "data.json"), 'r') as files:
            heroDAO.liste= json.loads(files.read())
        
        return heroDAO.liste
    
    @staticmethod
    def add_hero(name, characteristics, image_name, image):
        image_name = str(name) + ".png"
        with open(os.path.join(settings.STATICFILES_DIRS[0], "list", "images", image_name) , 'wb+') as dest:
            for chunk in image.chunks():
                dest.write(chunk)
        
        personnage = {"name": name, "characteristics":characteristics, "photo":"images/"+image_name}
        
        db_content = heroDAO.get_hero()

        db_content.append(personnage)

        with open(os.path.join(settings.STATICFILES_DIRS[0], "list", "data.json"), 'w') as files:
            files.write(json.dumps(db_content))

        