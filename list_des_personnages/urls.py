from django.urls import path
from . import views

urlpatterns = [
    path('', views.heros),
    path('add_new_personnage', views.add_hero)
]