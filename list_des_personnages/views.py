from django.shortcuts import render

from .DAO import heroDAO

# Create your views here.

from django.http import JsonResponse

def heros(request):
    return render(request, "list/liste_des_personnages.html", context = {"personnages": heroDAO.get_hero()})

def add_hero(request):
    image = request.FILES['image_du_personnage']

    name = request.POST.get("nom_du_personnage")

    characteristics = request.POST.get("characteristics")

    if None in (image, name, characteristics):
        return JsonResponse({"status":"failure: not enough args: " + str((image, name, characteristics))})

    image_name = name + ".png"

    try:
        heroDAO.add_hero(name, characteristics, image_name, image)
        return JsonResponse({"status":"Success"})

    except Exception as err:
        return JsonResponse({"status":"failure"})


